package com.beveragesapp;

public class BeverageOrder {
    private String  beverageType,beverageSize, beverageFlavor;
    private double beveragePrice, totalPrice;
    private final double SMALL_SIZE_BEVERAGE_PRICE = 1.50, MEDIUM_SIZE_BEVERAGE_PRICE = 2.50, LARGEE_LARGE_BEVERAGE_PRICE = 3.25;
    private final double COFFEE_FLAVOR_VANILA = 0.25, COFFEE_FLAVOR_CHOCOLATE = 0.75;
    private final double TEA_FLAVOR_LEMON = 0.25, TEA_FLAVOR_MINT=0.50,NONE=0.00;


    //getter Method of BeverageType
    public String getBeverageType() {
        return this.beverageType;
    }

    public void setBeverageType(String beverageType) {
        this.beverageType = beverageType;
    }

    public boolean isValidBeverageType(String beveragetype) {

        if (beveragetype.equalsIgnoreCase("coffee") || beveragetype.equalsIgnoreCase("c") || beveragetype.equalsIgnoreCase("tea") || beveragetype.equalsIgnoreCase("t"))
            return true;
        else
            return false;
    }

    public String getBeverageSize() {
        return this.beverageSize;
    }

    public void setBeverageSize(String beverageSize) {
        this.beverageSize = beverageSize;
    }

    public boolean isValidBeverageSize(String beverageSize) {

        if (beverageSize.equalsIgnoreCase("small") || beverageSize.equalsIgnoreCase("s") ||
                beverageSize.equalsIgnoreCase("medium") || beverageSize.equalsIgnoreCase("m") ||
                beverageSize.equalsIgnoreCase("large") || beverageSize.equalsIgnoreCase("l"))
            return true;
        else
            return false;
    }

    public String getBeverageFlavor() {
        return this.beverageFlavor;
    }

    public void setBeverageFlavor(String beverageFlavor) {
        this.beverageFlavor = beverageFlavor;
    }

    public boolean isValidBeverageFlavor(String beverageFlavor) {
        //boolean isflavortype = false;
        switch (this.beverageType.toLowerCase())
        {
            case "c":
            case "coffee" :
                if (beverageFlavor.equalsIgnoreCase("none") ||
                    beverageFlavor.equalsIgnoreCase("vanilla") || beverageFlavor.equalsIgnoreCase("v") ||
                    beverageFlavor.equalsIgnoreCase("chocolate") || beverageFlavor.equalsIgnoreCase("c") )
                    return true;
                else
                    return false;
            case "t":
            case "tea":
                if (beverageFlavor.equalsIgnoreCase("none") ||
                    beverageFlavor.equalsIgnoreCase("lemon") ||  beverageFlavor.equalsIgnoreCase("l") ||
                    beverageFlavor.equalsIgnoreCase("mint") || beverageFlavor.equalsIgnoreCase("m"))
                    return true;
                else
                    return false;
             default : return false;
        }

//        if (this.beverageType.equalsIgnoreCase("coffee")) {
//            if (beverageFlavor.equalsIgnoreCase("none") || beverageFlavor.equalsIgnoreCase("vanilla") || beverageFlavor.equalsIgnoreCase("chocolate"))
//                return isflavortype = true;
//            else
//                return isflavortype = false;
//        }
//        else if (this.beverageType.equals("tea")) {
//            if (beverageFlavor.equalsIgnoreCase("none") || beverageFlavor.equalsIgnoreCase("lemon") || beverageFlavor.equalsIgnoreCase("mint"))
//                return isflavortype = true;
//            else
//                return isflavortype = false;
//        }
//        return isflavortype;
    }

    private double getPrice() {
        double sizeprice=0,flavorPrice=0;
        switch(this.beverageSize.toLowerCase())
        {
            case "s":
            case "small":
                sizeprice = SMALL_SIZE_BEVERAGE_PRICE;
                break;
            case "m" :
            case "medium" :
                sizeprice = MEDIUM_SIZE_BEVERAGE_PRICE;
                break;
            case "l" :
            case "large" :
                sizeprice = LARGEE_LARGE_BEVERAGE_PRICE;
        }

       switch (this.beverageFlavor.toLowerCase())
       {
           case "c" :
           case "chocolate" :
               flavorPrice =  COFFEE_FLAVOR_CHOCOLATE;
                  break;
           case "v" :
           case "vanilla" :
               flavorPrice =  COFFEE_FLAVOR_VANILA;
               break;
           case "l" :
           case "lemon" :
               flavorPrice =  TEA_FLAVOR_LEMON;
               break;
           case "m" :
           case "mint" :
               flavorPrice =  TEA_FLAVOR_MINT;
               break;
           case "none" :
               flavorPrice =  NONE;
               break;

       }
        return sizeprice + flavorPrice;
    }

    public double getTotalPrice()
    {
        beveragePrice = getPrice();
        totalPrice = beveragePrice + (beveragePrice*0.13);
        return Math.round(totalPrice*100.0)/100.0;
    }

}
