package com.beveragesapp;
import javax.swing.JOptionPane;

public class Lab1BeveragesApplication {

    public static void main(String[] args) {

        String choice;
        do {

            BeverageOrder obj_BeverageOrder = new BeverageOrder();
            String inputCustomerName, inputBeverageType, inputBeverageSize, inputBeverageFlavor;

            inputCustomerName = JOptionPane.showInputDialog("Enter Customer Name:");

            boolean isValidType = false;
            do {
                inputBeverageType = JOptionPane.showInputDialog("Please Enter Beverage Type(Coffee/Tea)");

                //check  beverage type is valid or not
                isValidType = obj_BeverageOrder.isValidBeverageType(inputBeverageType);
                if (isValidType)
                    obj_BeverageOrder.setBeverageType(inputBeverageType);
                else
                    JOptionPane.showMessageDialog(null, "Please enter valid Beverage Type");
            } while (!isValidType);


            //check beverage size is valid or not
            boolean isValidSize = false;
            do {
                inputBeverageSize = JOptionPane.showInputDialog("Please Enter Beverage Size(Small/Medium/Large):");

                isValidSize = obj_BeverageOrder.isValidBeverageSize(inputBeverageSize);

                if (isValidSize)
                    obj_BeverageOrder.setBeverageSize(inputBeverageSize);
                else
                    JOptionPane.showMessageDialog(null, "Please Enter Valid Beverage Size");
            } while (!isValidSize);

            boolean isValidFlavor = false;
            do {
                inputBeverageFlavor = JOptionPane.showInputDialog("Please Enter " + inputBeverageType + " Flavor:");

                isValidFlavor = obj_BeverageOrder.isValidBeverageFlavor(inputBeverageFlavor);
                if (isValidFlavor)
                    obj_BeverageOrder.setBeverageFlavor(inputBeverageFlavor);
                else
                    JOptionPane.showMessageDialog(null, "Please Enter Valid Flavor for" + inputBeverageType);

            } while (!isValidFlavor);

            String orderDetailMessage = "For " + inputCustomerName + ", a " + obj_BeverageOrder.getBeverageSize() + " " + obj_BeverageOrder.getBeverageType() + ", " + obj_BeverageOrder.getBeverageFlavor() + " flavoring, cost: $ " + obj_BeverageOrder.getTotalPrice() + ".";
            JOptionPane.showMessageDialog(null, orderDetailMessage);

            choice = JOptionPane.showInputDialog(null, "Would You Like To Take Another Order(Y/N)?");
        }while (choice.equalsIgnoreCase("Y"));

    }
}

